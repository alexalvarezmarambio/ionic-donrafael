import {HttpHeaders} from '@angular/common/http';

export const environment = {
  production: true,
  api: 'https://us-central1-donrafael-cb5b7.cloudfunctions.net/api',
  headers: new HttpHeaders({
    Authorization: 'cXBoYXJtYTIwMjA6WA==',
  }),
  firebase: {
    apiKey: "AIzaSyCdrBLIVXDPnQb0BM21t-kCB743K49AFl0",
    authDomain: "donrafael-cb5b7.firebaseapp.com",
    databaseURL: "https://donrafael-cb5b7.firebaseio.com",
    projectId: "donrafael-cb5b7",
    storageBucket: "donrafael-cb5b7.appspot.com",
    messagingSenderId: "101190723772",
    appId: "1:101190723772:web:fe0c48260c9cc273d388b9"
  }
};

