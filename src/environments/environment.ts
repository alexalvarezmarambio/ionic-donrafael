// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {HttpHeaders} from '@angular/common/http';

export const environment = {
  production: false,
  api: 'https://us-central1-donrafael-cb5b7.cloudfunctions.net/api',
  //api: 'http://localhost:5000/donrafael-cb5b7/us-central1/api',
  headers: new HttpHeaders({
    Authorization: 'cXBoYXJtYTIwMjA6WA==',
  }),
  firebase: {
    apiKey: "AIzaSyCdrBLIVXDPnQb0BM21t-kCB743K49AFl0",
    authDomain: "donrafael-cb5b7.firebaseapp.com",
    databaseURL: "https://donrafael-cb5b7.firebaseio.com",
    projectId: "donrafael-cb5b7",
    storageBucket: "donrafael-cb5b7.appspot.com",
    messagingSenderId: "101190723772",
    appId: "1:101190723772:web:fe0c48260c9cc273d388b9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
