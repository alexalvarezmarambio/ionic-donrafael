import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {CajaModel} from '../models/caja.model';
import {CuadreModel} from '../models/cuadre.model';
import {CuadreService} from './services/cuadre.service';
import {VentaModel} from '../models/venta.model';
import {AlertController, ToastController} from '@ionic/angular';
import {CajaService} from '../cajas/services/caja.service';

@Component({
  selector: 'app-cuadre',
  templateUrl: './cuadre.page.html',
  styleUrls: ['./cuadre.page.scss'],
})
export class CuadrePage implements OnInit {

  caja: CajaModel;
  cuadre: CuadreModel = {
        ventas: 0,
        salidas: 0,
        remesas: 0,
        sistema: 0,
        vendedor: 0,
        diferencia: 0
      };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public cuadreService: CuadreService,
    public cajaService: CajaService,
    private alertController: AlertController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.cajaService.caja(params.id).subscribe(
        res => {
          this.caja = res;
          this.obtenerVentasCaja();
        }
      );
    });
  }

  obtenerVentasCaja() {
    this.cuadreService.obtnerVentasCaja(this.caja.id).subscribe(
      response => {
        this.cuadre.ventas = 0;
        this.cuadre.remesas = 0;
        this.cuadre.salidas = 0;
        this.cuadre.sistema = 0;
        this.cuadre.vendedor = 0;
        this.cuadre.diferencia = 0;
        console.log('ventas', response);
        this.cuadre.ventas = this.sumarVentas(response);
        this.cuadre.remesas = this.sumarRemesas();
        this.cuadre.salidas = this.sumarSalidas();
        this.cuadre.sistema = this.sumarSistema();
        this.cuadre.vendedor = this.sumarVendedor();
        this.cuadre.diferencia = this.sumarDiferencia();
      },
      error => this.mostrarToast(error)
    );
    
  }

  async actualizarCuadre() {

    const alerta = await this.alertController.create({
      header: 'Confirmar',
      message: 'Desea realizar esta accion?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Aceptar',
          handler: async () => {
            try {
              const promesas = [
                this.cajaService.actualizarCuadre(this.caja.id, this.cuadre),
                this.cajaService.acutalizarEstado(this.caja.id, '2')
              ];

              await Promise.all(promesas);
              this.mostrarToast('Caja Cuadrada');
              this.router.navigate(['/layout/cajas']);
            } catch (error) {
              this.mostrarToast(error.message);
            }
          }
        }
      ]
    });

    await alerta.present();

  }

  sumarDiferencia() {
    return this.cuadre.vendedor - this.cuadre.sistema;
  }

  sumarVendedor() {
    return this.caja.fondo + this.cuadre.remesas - this.cuadre.salidas;
  }

  sumarSistema() {
    return this.caja.fondo + this.cuadre.ventas - this.cuadre.salidas;
  }

  sumarVentas(ventas: VentaModel[]) {
    return ventas.reduce((acumulator, value) => acumulator + value.total, this.cuadre.ventas);
  }

  sumarSalidas() {
    return this.caja.salidas.reduce((acumulator, value) => acumulator + value.monto, this.cuadre.salidas);
  }

  sumarRemesas() {
    return this.caja.remesas.reduce((acumulator, value) => acumulator + value.monto, this.cuadre.remesas);
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toastController.create({
      duration: 3000,
      message: mensaje
    });

    await toast.present();

  }

}
