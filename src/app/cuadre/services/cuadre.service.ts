import { Injectable } from '@angular/core';
import {VentaModel} from 'src/app/models/venta.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CuadreService {

  constructor(
    private aff: AngularFirestore
  ) { }

  obtnerVentasCaja(id: string): Observable<VentaModel[]> {
    return this.aff.collection<VentaModel>('ventas', ref => ref.where('caja.id', '==', id)).valueChanges({idField: 'id'});
  }
}
