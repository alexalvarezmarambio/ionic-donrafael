import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevoVendedorPage } from './nuevo-vendedor.page';

const routes: Routes = [
  {
    path: '',
    component: NuevoVendedorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevoVendedorPageRoutingModule {}
