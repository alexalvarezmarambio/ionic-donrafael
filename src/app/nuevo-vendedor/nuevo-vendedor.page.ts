import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { VendedorModel } from '../models/vendedor.model';
import { VendorServiceService } from '../vendedores/vendor-service.service';

@Component({
  selector: 'app-nuevo-vendedor',
  templateUrl: './nuevo-vendedor.page.html',
  styleUrls: ['./nuevo-vendedor.page.scss'],
})
export class NuevoVendedorPage implements OnInit {
  vendorForm = this.fb.group({
    nombre: ['', Validators.required],
    clave: ['1234'],
    estado: [true, Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private toast: ToastController,
    public vendorService: VendorServiceService
  ) { }

  ngOnInit() {
  }

  nuevoVendor() {
    if (this.vendorForm.invalid) {
      this.mostrarToast('Formulario Invalido');
      return;
    }

    const newVendor: VendedorModel = this.vendorForm.value;
    newVendor.nombre = newVendor.nombre.toUpperCase();

    this.vendorService.nuevoVendedor(newVendor)
    .then(res => {
      this.mostrarToast('Vendedor Creado');
      this.vendorForm.reset();
    })
    .catch(error => this.mostrarToast(error));
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 8000,
      message: mensaje
    });

    toast.present();

  }

}
