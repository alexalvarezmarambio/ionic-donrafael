import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NuevoVendedorPage } from './nuevo-vendedor.page';

describe('NuevoVendedorPage', () => {
  let component: NuevoVendedorPage;
  let fixture: ComponentFixture<NuevoVendedorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoVendedorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NuevoVendedorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
