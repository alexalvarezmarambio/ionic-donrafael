import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { Validators, FormBuilder } from '@angular/forms';
import { ProductoModel } from '../models/producto.model';
import { ToastController } from '@ionic/angular';
import { ProductoService } from '../productos/services/producto.service';
import { CategoryService } from '../categorias/service/category.service';
import { Observable, of, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {

  loading = false;
  guardar = true;
  actualizar = false;
  barcode = '';
  productId = '';
  productForm = this.fb.group({
    producto: ['', Validators.required],
    barcode: ['', Validators.required],
    precio: ['', [Validators.required, Validators.min(10)]],
    categoria: ['', Validators.required],
    medida: ['', Validators.required]
  });
  categorias: Observable<any>;

  constructor(
    private fb: FormBuilder,
    private barcodeScanner: BarcodeScanner,
    private toast: ToastController,
    public main: ProductoService,
    public categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.categorias = this.categoryService.categorias();
  }

  async scan() {
    this.productForm.reset();

    const barra = await this.barcodeScanner.scan();
    let update = false;
    this.main.obtenerProducto(barra.text).subscribe(
      (productos) => {
        if (0 in productos) {
          update = true;
        }

        this.setearOperacion(update, barra.text, productos[0]);
      },
      error => this.mostrarToast(error)
    );
  }

  productoExiste(barra: string) {
    return this.main.obtenerProductoGet(barra).pipe(
      switchMap(res => {
        if(0 in res.docs){
          return of(true);
        } else {
          return of(false);
        }
      })
    );
  }

  setearOperacion(update: boolean, barra: string, producto?: ProductoModel) {
    
    if (update) {
      this.productId = producto.id;
      this.productForm.controls.barcode.setValue(barra);
      this.productForm.controls.producto.setValue(producto.name);
      this.productForm.controls.categoria.setValue(producto.category);
      this.productForm.controls.precio.setValue(producto.kilo <= 10 ? producto.precio : producto.kilo);
      this.productForm.controls.medida.setValue(producto.kilo <= 10 ? 'U' : 'K');

      this.guardar = false;
      this.actualizar = true;
    } else {
      this.productForm.controls.barcode.setValue(barra);
      this.actualizar = false;
      this.guardar = true;
    }
  }

  async guardarProducto() {

    const barra = this.productForm.controls.barcode.value;
    const existe = await this.productoExiste(barra).toPromise();
    if (existe) {
      this.mostrarToast('Producto ya existe');
      this.productForm.reset();
      return;
    }

    const medida = this.productForm.controls.medida.value;
    this.loading = true;

    const producto = {
      barcode : this.productForm.controls.barcode.value,
      category : this.productForm.controls.categoria.value,
      cost : 0,
      kilo : medida === 'K' ? Number.parseInt(this.productForm.controls.precio.value.toString()) : 0,
      margin : 0,
      name : this.productForm.controls.producto.value.toString().toUpperCase(),
      neto : 0,
      precio : medida === 'U' ? Number.parseInt(this.productForm.controls.precio.value.toString()) : 0,
      stock : 100,
    };

    await this.main.crearProducto(producto);
    this.mostrarToast('Producto Creado.');
    this.productForm.reset();
    this.loading = false;
  }

  async actualizarProducto() {
    try {
      const medida = this.productForm.controls.medida.value;
      this.loading = true;
      const body = {
        name: this.productForm.controls.producto.value.toString().toUpperCase(),
        kilo : medida === 'K' ? Number.parseInt(this.productForm.controls.precio.value.toString()) : 0,
        precio : medida === 'U' ? Number.parseInt(this.productForm.controls.precio.value.toString()) : 0,
        category: this.productForm.controls.categoria.value
      }

      await this.main.actualizarProducto(this.productId, body);
      this.mostrarToast('Producto Actualizado.');
      this.productForm.reset();
      this.loading = false;
      this.guardar = true;
      this.actualizar = false;

    } catch (error) {
      this.mostrarToast(error.message);
      this.loading = false;
      this.guardar = true;
      this.actualizar = false;
    }
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 8000,
      message: mensaje
    });

    toast.present();

  }

}
