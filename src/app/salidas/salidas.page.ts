import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CajaService} from '../cajas/services/caja.service';
import {CajaModel} from '../models/caja.model';
import {AlertController, ToastController} from '@ionic/angular';

@Component({
  selector: 'app-salidas',
  templateUrl: './salidas.page.html',
  styleUrls: ['./salidas.page.scss'],
})
export class SalidasPage implements OnInit {

  caja: CajaModel;

  constructor(
    private route: ActivatedRoute,
    public cajaService: CajaService,
    private alertController: AlertController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
    this.recibirParamentros();
  }

  recibirParamentros() {
    this.route.params.subscribe(params => {
      this.cajaService.caja(params.id).subscribe(
        res => this.caja = res
      );
    });
  }

  async eliminarSalida(index: number) {

    const alerta = await this.alertController.create({
      header: 'Confirmar',
      message: 'Desea realizar esta operacion?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Aceptar',
          handler: async () => {
            const salidasCopy = [...this.caja.salidas];
            salidasCopy.splice(index, 1);
            try {
              await this.cajaService.actualizarSalida(this.caja.id, salidasCopy);
              this.mostrarToast('Salida Borrada.');
            } catch (error) {
              this.mostrarToast(error.message);
            }

          }
        }
      ]
    });

    await alerta.present();

  }

  calcularTotal() {
    return this.caja.salidas.reduce( (acumulator, value) => acumulator + value.monto, 0 );
  }

  async mostrarToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 3000
    });

    toast.present();
  }

}
