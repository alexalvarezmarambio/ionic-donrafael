import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MainService } from '../services/main.service';

import { ToastController } from '@ionic/angular';
import {UsuarioModel} from '../models/usuario.model';
import {Router} from '@angular/router';
import {AuthService} from './services/auth.service';
import {CredencialesModel} from '../models/credenciales.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  loginForm = new FormGroup({
    usuario: new FormControl('', Validators.required),
    clave: new FormControl('', Validators.required),
  });
  usuario: UsuarioModel;

  constructor(
    public mainService: MainService,
    public authService: AuthService,
    private toast: ToastController,
    private router: Router,
  ) {}

  async ngOnInit() {
    await this.mainService.auth();
    const usuarioStorage = this.obtenerLocalStorage();
    if (usuarioStorage !== null) {
      this.loginForm.controls.usuario.setValue(usuarioStorage.usuario);
      this.loginForm.controls.clave.setValue(usuarioStorage.clave);
      
      this.login();
    }
  }

  obtenerLocalStorage() {
    const usuarioStorage = JSON.parse(localStorage.getItem('usuario'));
    return usuarioStorage;
  }

  guardarLocalStorage() {
    const usuarioStorage = {
      usuario: this.loginForm.controls.usuario.value,
      clave: this.loginForm.controls.clave.value,
    };

    localStorage.setItem('usuario', JSON.stringify(usuarioStorage));
  }

  login() {
    
    const credencial: CredencialesModel = {
      usuario: this.loginForm.controls.usuario.value,
      clave: this.loginForm.controls.clave.value 
    };
    
    this.authService.autenticacion( credencial ).subscribe(
      response => {
        if ( 0 in response ) {
          this.usuario = response[0];
          this.mainService.usuario = this.usuario;
          this.guardarLocalStorage();
          this.router.navigate(['/layout']);
        } else {
          this.mostrarToast('Usuario y/o clave incorrectos.');
        }
      },
      error => console.log(error)
    );
  }

  async mostrarToast(mensaje: string) {
      const notificar = await this.toast.create({
        duration: 3000,
        message: mensaje,
      });

      notificar.present();
  }

}
