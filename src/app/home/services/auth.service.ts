import { Injectable } from '@angular/core';
import {CredencialesModel} from 'src/app/models/credenciales.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private aff: AngularFirestore
  ) { }

  autenticacion(credencial: CredencialesModel): Observable<any> {
    return this.aff.collection('usuarios', ref => ref
    .where('usuario', '==', credencial.usuario)
    .where('clave', '==', credencial.clave))
    .valueChanges({idField: 'id'});
  }
}
