import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateCategoriaPageRoutingModule } from './update-categoria-routing.module';

import { UpdateCategoriaPage } from './update-categoria.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    UpdateCategoriaPageRoutingModule
  ],
  declarations: [UpdateCategoriaPage]
})
export class UpdateCategoriaPageModule {}
