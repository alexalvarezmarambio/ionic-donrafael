import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateCategoriaPage } from './update-categoria.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateCategoriaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateCategoriaPageRoutingModule {}
