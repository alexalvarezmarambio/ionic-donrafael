import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { CategoryService } from '../categorias/service/category.service';
import { Categoria } from '../models/categoria.model';

@Component({
  selector: 'app-update-categoria',
  templateUrl: './update-categoria.page.html',
  styleUrls: ['./update-categoria.page.scss'],
})
export class UpdateCategoriaPage implements OnInit {

  catForm = this.fb.group({
    id: [''],
    nombre: ['', Validators.required],
    estado: ['', Validators.required],
    pos: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private toast: ToastController,
    public catService: CategoryService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.catService.categoria(params.id).subscribe(
        res => {
          this.catForm.setValue(res);
        }
      );
    });
  }

  updateCat() {
    if (this.catForm.invalid) {
      this.mostrarToast('Formulario Invalido');
      return
    }

    const {id, ...update} = this.catForm.value;
    update.nombre = update.nombre.toUpperCase();

    this.catService.updateCategoria(id, update)
    .then(res => {
      this.mostrarToast('Categoria Actualizada');
      this.catForm.reset();
    })
    .catch(error => this.mostrarToast(error));
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 8000,
      message: mensaje
    });

    toast.present();

  }

}
