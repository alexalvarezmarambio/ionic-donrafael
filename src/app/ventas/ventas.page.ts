import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import * as dayjs from 'dayjs';
import { VentaModel } from '../models/venta.model';
import { VentasService } from './services/ventas.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.page.html',
  styleUrls: ['./ventas.page.scss'],
})
export class VentasPage implements OnInit {
  ventaForm = this.fb.group({
    dia: ['', Validators.required]
  });
  ventas: VentaModel[] = [];

  constructor(
    private fb: FormBuilder,
    public saleService: VentasService
  ) { }

  ngOnInit() {
  }

  buscar() {
    const fecha = dayjs(this.ventaForm.controls.dia.value).format('YYYY-MM-DD');
    this.saleService.buscarVentas(fecha).subscribe(
      res => this.ventas = res,
      error => console.log(error)
    );
  }

}
