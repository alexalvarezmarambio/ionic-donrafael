import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { VentaModel } from 'src/app/models/venta.model';

@Injectable({
  providedIn: 'root'
})
export class VentasService {

  constructor(
    private aff: AngularFirestore
  ) { }

  buscarVentas(fecha: string): Observable<VentaModel[]> {
    return this.aff.collection<VentaModel>(
      'ventas',
      ref => ref
        .where('fecha', '>=', fecha + ' 00:00:00')
        .where('fecha', '<=', fecha + ' 23:59:59')
    ).valueChanges({idField: 'id'});
  }

  venta(id: string): Observable<VentaModel> {
    return this.aff.doc<VentaModel>(`ventas/${id}`).valueChanges({idField: 'id'});
  }
}
