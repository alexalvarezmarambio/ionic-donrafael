import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Proveedor } from 'src/app/models/proveedor.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  constructor(
    private aff: AngularFirestore
    ) {}

  obtenerProveedores(): Observable<Proveedor[]> {
    return this.aff.collection<Proveedor>('proveedores', ref => ref.orderBy('nombre', 'asc')).valueChanges({idField: 'id'});
  }

  crearProveedor(body: Proveedor) {
    return this.aff.collection('proveedores').add(body);
  }

  proveedor(id: string): Observable<Proveedor> {
    return this.aff.doc<Proveedor>(`proveedores/${id}`).valueChanges({idField: 'id'});
  }

  updateProveedor(id: string, prov: Proveedor) {
    return this.aff.doc(`proveedores/${id}`).update(prov);
  }
}
