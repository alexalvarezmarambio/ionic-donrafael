import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Proveedor } from '../models/proveedor.model';
import { ProveedorService } from './services/proveedor.service';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.page.html',
  styleUrls: ['./proveedores.page.scss'],
})
export class ProveedoresPage implements OnInit {
  proveedores: Observable<Proveedor[]>;

  constructor(
    public provService: ProveedorService
  ) { }

  ngOnInit() {
    this.proveedores = this.provService.obtenerProveedores();
  }
}
