import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { CategoryService } from '../categorias/service/category.service';
import { Categoria } from '../models/categoria.model';

@Component({
  selector: 'app-nueva-categoria',
  templateUrl: './nueva-categoria.page.html',
  styleUrls: ['./nueva-categoria.page.scss'],
})
export class NuevaCategoriaPage implements OnInit {

  catForm = this.fb.group({
    nombre: ['', Validators.required],
    estado: ['', Validators.required],
    pos: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private toast: ToastController,
    public catService: CategoryService
  ) { }

  ngOnInit() {
  }

  nuevaCat() {
    if (this.catForm.invalid) {
      return;
    }

    const newCat: Categoria = this.catForm.value;
    newCat.nombre = newCat.nombre.toUpperCase();
    this.catService.crearCategorias(newCat)
    .then(res => {
      this.mostrarToast('Categoria Creada');
      this.catForm.reset();
    })
    .catch(error => this.mostrarToast(error));
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 8000,
      message: mensaje
    });

    toast.present();

  }

}
