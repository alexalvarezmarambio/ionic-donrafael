import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateFondoPageRoutingModule } from './update-fondo-routing.module';

import { UpdateFondoPage } from './update-fondo.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    UpdateFondoPageRoutingModule
  ],
  declarations: [UpdateFondoPage]
})
export class UpdateFondoPageModule {}
