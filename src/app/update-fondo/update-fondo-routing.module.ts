import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateFondoPage } from './update-fondo.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateFondoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateFondoPageRoutingModule {}
