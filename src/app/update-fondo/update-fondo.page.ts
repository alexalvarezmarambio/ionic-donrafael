import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CajaService } from '../cajas/services/caja.service';
import { CajaModel } from '../models/caja.model';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-update-fondo',
  templateUrl: './update-fondo.page.html',
  styleUrls: ['./update-fondo.page.scss'],
})
export class UpdateFondoPage implements OnInit {
  fondoForm = this.fb.group({
    fondo: ['', [Validators.required, Validators.min(0)]]
  });
  caja: CajaModel;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private toast: ToastController,
    public boxService: CajaService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params =>{
      this.boxService.caja(params.id).subscribe(
        res => {
          this.caja = res
          this.fondoForm.controls.fondo.setValue(this.caja.fondo);
        }
      );
    });
  }

  updateFondo() {
    if (this.fondoForm.invalid) {
      this.mostrarToast('Formulario Invalido');
      return;
    }

    const fondo = Number.parseInt(this.fondoForm.controls.fondo.value, 10);
    this.boxService.actualizarFondo(this.caja.id, fondo)
    .then(res => {
      this.mostrarToast('Fondo Actualizado');
      this.fondoForm.reset();
    })
    .catch(error => this.mostrarToast(error));
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 8000,
      message: mensaje
    });

    toast.present();

  }

}
