import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {VendedorModel} from '../models/vendedor.model';
import {ToastController} from '@ionic/angular';
import * as dayjs from 'dayjs';
import {CajaService} from '../cajas/services/caja.service';
import { VendorServiceService } from '../vendedores/vendor-service.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-crear-caja',
  templateUrl: './crear-caja.page.html',
  styleUrls: ['./crear-caja.page.scss'],
})
export class CrearCajaPage implements OnInit {

  cajaForm = this.fb.group({
    apertura: [dayjs().format('YYYY-MM-DD HH:mm:ss')],
    cierre: [''],
    fondo: [0, Validators.min(0)],
    estado: ['0'],
    vendedor: ['', Validators.required],
    remesas: [[]],
    salidas: [[]]
  });

  vendedores: Observable<VendedorModel[]>;

  constructor(
    private fb: FormBuilder,
    private toastController: ToastController,
    public cajaService: CajaService,
    public vendorService: VendorServiceService
  ) { }

  ngOnInit() {
    this.vendedores = this.vendorService.vendedoresActivos();
  }

  crearCaja() {
  
    const vendedor = this.cajaForm.controls.vendedor.value;
    console.log(vendedor.nombre);
    this.cajaService.obtenerCajaClave(vendedor.nombre).subscribe(
      async cajas => {
        if (0 in cajas) {
          this.mostrarToast('Caja ya Abierta.');
          return;
        }
        
        await this.cajaService.crearCaja(this.cajaForm.value);
        this.mostrarToast('Caja Creada.');
      },
      error => this.mostrarToast(error)
    );
  }

  async mostrarToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 8000
    });

    await toast.present();

  }

}
