import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearCajaPage } from './crear-caja.page';

const routes: Routes = [
  {
    path: '',
    component: CrearCajaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearCajaPageRoutingModule {}
