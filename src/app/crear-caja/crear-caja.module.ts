import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearCajaPageRoutingModule } from './crear-caja-routing.module';

import { CrearCajaPage } from './crear-caja.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    CrearCajaPageRoutingModule
  ],
  declarations: [CrearCajaPage]
})
export class CrearCajaPageModule {}
