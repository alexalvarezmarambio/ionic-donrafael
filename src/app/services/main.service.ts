import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {UsuarioModel} from '../models/usuario.model';


@Injectable({
  providedIn: 'root'
})
export class MainService {

  usuario: UsuarioModel;

  constructor(
    private afa: AngularFireAuth
  ) { }

  auth() {
    return this.afa.signInWithEmailAndPassword('alexalvarezmarambio@gmail.com', '123456');
  }
}
