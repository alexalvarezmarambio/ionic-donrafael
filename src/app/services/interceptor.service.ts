import { Injectable } from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const { api, headers } = environment;

    const reqClonada = req.clone({
      url: api + req.url,
      headers,
    });

    return next.handle(reqClonada).pipe(
      catchError(this.manejaError),
    );
  }

  manejaError(error: HttpErrorResponse) {
    return throwError(error.message);
  }
}
