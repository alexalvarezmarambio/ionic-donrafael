import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevoProvPage } from './nuevo-prov.page';

const routes: Routes = [
  {
    path: '',
    component: NuevoProvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevoProvPageRoutingModule {}
