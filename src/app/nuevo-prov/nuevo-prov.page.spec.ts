import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NuevoProvPage } from './nuevo-prov.page';

describe('NuevoProvPage', () => {
  let component: NuevoProvPage;
  let fixture: ComponentFixture<NuevoProvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoProvPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NuevoProvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
