import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Proveedor } from '../models/proveedor.model';
import { ProveedorService } from '../proveedores/services/proveedor.service';

@Component({
  selector: 'app-nuevo-prov',
  templateUrl: './nuevo-prov.page.html',
  styleUrls: ['./nuevo-prov.page.scss'],
})
export class NuevoProvPage implements OnInit {
  provForm = this.fb.group({
    nombre: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    public provService: ProveedorService
  ) { }

  ngOnInit() {
  }


  async nuevoProv() {
    const nuevoProv: Proveedor = this.provForm.value;
    nuevoProv.nombre = nuevoProv.nombre.toUpperCase();

    try {
      await this.provService.crearProveedor(nuevoProv);
      this.router.navigateByUrl('/layout/proveedores');
    } catch (error) {
      console.log(error);
    }
  }

}
