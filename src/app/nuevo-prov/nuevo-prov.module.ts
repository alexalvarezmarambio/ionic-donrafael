import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevoProvPageRoutingModule } from './nuevo-prov-routing.module';

import { NuevoProvPage } from './nuevo-prov.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    NuevoProvPageRoutingModule
  ],
  declarations: [NuevoProvPage]
})
export class NuevoProvPageModule {}
