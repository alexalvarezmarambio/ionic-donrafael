import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RemesasPage } from './remesas.page';

describe('RemesasPage', () => {
  let component: RemesasPage;
  let fixture: ComponentFixture<RemesasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemesasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RemesasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
