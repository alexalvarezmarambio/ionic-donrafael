import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { CajaService } from '../cajas/services/caja.service';
import { CajaModel } from '../models/caja.model';

@Component({
  selector: 'app-remesas',
  templateUrl: './remesas.page.html',
  styleUrls: ['./remesas.page.scss'],
})
export class RemesasPage implements OnInit {

  caja: CajaModel;

  constructor(
    private route: ActivatedRoute,
    private alertCtrl: AlertController,
    private toast: ToastController,
    public boxService: CajaService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.boxService.caja(params.id).subscribe(
        res => this.caja = res
      );
    });
  }

  async eliminarRemesa(index: number) {
    const alerta = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Desea realizar esta operacion?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Aceptar',
          handler: async () => {
            const remesasCopy = [...this.caja.remesas];
            remesasCopy.splice(index, 1);
            try {
              await this.boxService.actualizarRemesas(this.caja.id, remesasCopy);
              this.mostrarToast('Remesa Borrada.');          
            } catch (error) {
              this.mostrarToast(error.message);
            }

          }
        }
      ]
    });

    await alerta.present();
  }

  calcularTotal() {
    return this.caja.remesas.reduce((accu, value) => accu + value.monto, 0);
  }

  async mostrarToast(mensaje: string) {
    const toast = await this.toast.create({
      message: mensaje,
      duration: 8000
    });

    toast.present();
  }

}
