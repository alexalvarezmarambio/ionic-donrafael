import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryService } from './service/category.service';
import { Categoria } from '../models/categoria.model';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.page.html',
  styleUrls: ['./categorias.page.scss'],
})
export class CategoriasPage implements OnInit {

  categorias: Observable<Categoria[]>;

  constructor(
    public service: CategoryService
  ) { }

  ngOnInit() {
    this.categorias = this.service.categorias();
  }

}
