import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Categoria } from '../../models/categoria.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(
    private afs: AngularFirestore
  ) { }

  categorias(): Observable<Categoria[]> {
    return this.afs.collection<Categoria>('categorias').valueChanges({ idField: 'id'});
  }

  crearCategorias(categoria: Categoria) {
    return this.afs.collection('categorias').add(categoria);
  }

  categoria(id: string): Observable<Categoria> {
    return this.afs.doc<Categoria>(`categorias/${id}`).valueChanges({ idField: 'id'});
  }

  updateCategoria(id: string, categoria: Categoria) {
    return this.afs.doc(`categorias/${id}`).update(categoria);
  }
}
