import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { VendedorModel } from '../models/vendedor.model';

@Injectable({
  providedIn: 'root'
})
export class VendorServiceService {

  constructor(
    private aff: AngularFirestore
  ) { }

  vendedores(): Observable<VendedorModel[]> {
    return this.aff.collection<VendedorModel>('vendors').valueChanges({idField: 'id'});
  }

  vendedoresActivos(): Observable<VendedorModel[]> {
    return this.aff.collection<VendedorModel>('vendors', ref => ref.where('estado', '==', true)).valueChanges({idField: 'id'});
  }

  vendedor(id: string): Observable<VendedorModel> {
    return this.aff.doc<VendedorModel>(`vendors/${id}`).valueChanges({idField: 'id'});
  }

  nuevoVendedor(vendedor: VendedorModel) {
    return this.aff.collection('vendors').add(vendedor);
  }

  updateVendedor(id: string, vendedor: VendedorModel) {
    return this.aff.doc(`vendors/${id}`).update(vendedor);
  }
}
