import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VendedoresPage } from './vendedores.page';

describe('VendedoresPage', () => {
  let component: VendedoresPage;
  let fixture: ComponentFixture<VendedoresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendedoresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VendedoresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
