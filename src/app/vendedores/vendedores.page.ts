import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { VendedorModel } from '../models/vendedor.model';
import { VendorServiceService } from './vendor-service.service';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.page.html',
  styleUrls: ['./vendedores.page.scss'],
})
export class VendedoresPage implements OnInit {
  vendedores: Observable<VendedorModel[]>;

  constructor(
    public vendorService: VendorServiceService
  ) { }

  ngOnInit() {
    this.vendedores = this.vendorService.vendedores();
  }



}
