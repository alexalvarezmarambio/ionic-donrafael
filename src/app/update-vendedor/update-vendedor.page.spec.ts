import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdateVendedorPage } from './update-vendedor.page';

describe('UpdateVendedorPage', () => {
  let component: UpdateVendedorPage;
  let fixture: ComponentFixture<UpdateVendedorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateVendedorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateVendedorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
