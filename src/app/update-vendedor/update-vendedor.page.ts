import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { VendedorModel } from '../models/vendedor.model';
import { VendorServiceService } from '../vendedores/vendor-service.service';

@Component({
  selector: 'app-update-vendedor',
  templateUrl: './update-vendedor.page.html',
  styleUrls: ['./update-vendedor.page.scss'],
})
export class UpdateVendedorPage implements OnInit {
  vendorForm = this.fb.group({
    id: [''],
    nombre: ['', Validators.required],
    clave: ['1234'],
    estado: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private toast: ToastController,
    public vendorService: VendorServiceService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.vendorService.vendedor(params.id).subscribe(
        res => this.vendorForm.setValue(res)
      )
    });
  }

  updateVendor() {
    if (this.vendorForm.invalid) {
      this.mostrarToast('Formulario Invalido');
      return;
    }

    const vendor: VendedorModel = this.vendorForm.value;
    const { id, ...update } = vendor;
    update.nombre = update.nombre.toUpperCase();

    this.vendorService.updateVendedor(id, update)
    .then(res => {
      this.mostrarToast('Vendedor Actualizado');
      this.vendorForm.reset();
    })
    .catch(error => this.mostrarToast(error));
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 8000,
      message: mensaje
    });

    toast.present();

  }

}
