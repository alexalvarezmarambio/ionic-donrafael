import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateVendedorPageRoutingModule } from './update-vendedor-routing.module';

import { UpdateVendedorPage } from './update-vendedor.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    UpdateVendedorPageRoutingModule
  ],
  declarations: [UpdateVendedorPage]
})
export class UpdateVendedorPageModule {}
