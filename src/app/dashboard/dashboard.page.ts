import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryService } from '../categorias/service/category.service';
import {ProductoService} from '../productos/services/producto.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  categorias: Observable<any>;

  constructor(
    public categoryService: CategoryService,
  ) { }

  ngOnInit() {
    this.categorias = this.categoryService.categorias();
  }

  

}
