import { Component, OnInit } from '@angular/core';
import { ProductoModel } from '../models/producto.model';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { ProductoService } from './services/producto.service';
import { Observable } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.page.html',
  styleUrls: ['./productos.page.scss'],
})
export class ProductosPage implements OnInit {

  isloader = false;
  productos: Observable<any>;
  filtrados: Observable<any>;

  filterForm = this.fb.group({
    key: ['']
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public main: ProductoService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.isloader = true;
      this.obtenerProductos(params.nombre);
    });
  }

  obtenerProductos(categoria: string) {
    try {
      this.productos = this.main.obtenerProductos(categoria);
      this.filtrados = this.productos;
      this.isloader = false;
    } catch (error) {
      console.log(error.message);
    }
  }

  actualizar(producto: ProductoModel) {
    
    this.router.navigate(['/layout/updateProducto', producto.id]);
  }

  filtrar() {
    const key = this.filterForm.controls.key.value.toString().toUpperCase();

    this.filtrados = new Observable();

    this.filtrados = this.productos.pipe(map((p: ProductoModel[]) => p.filter(pp => pp.name.includes(key))));
  }

}
