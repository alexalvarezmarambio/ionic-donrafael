import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ProductoModel } from '../../models/producto.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(
    private aff: AngularFirestore
  ) { }

  crearProducto(producto: ProductoModel) {
    return this.aff.collection('products').add(producto);
  }

  obtenerProducto(barra: string): Observable<any> {
    return this.aff.collection('products', ref => ref.where('barcode', '==', barra)).valueChanges({idField: 'id'});
  }

  obtenerProductoGet(barra: string): Observable<any> {
    return this.aff.collection('products', ref => ref.where('barcode', '==', barra)).get();
  }

  obtenerProductoId(id: string): Observable<any> {
    return this.aff.doc(`products/${id}`).valueChanges({idField: 'id'})
  }

  obtenerProductos(categoria: string) {
    return this.aff.collection('products', ref => 
      ref.where('category', '==', categoria)
      .orderBy('name', 'asc')
    ).valueChanges({idField: 'id'});
  }

  actualizarProducto(id: string, body: any) {
    return this.aff.doc(`products/${id}`).update(body);
  }

  eliminarProducto(id: string) {
    return this.aff.doc(`products/${id}`).delete();
  }
}
