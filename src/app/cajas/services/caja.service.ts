import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CajaModel} from 'src/app/models/caja.model';
import {CuadreModel} from 'src/app/models/cuadre.model';
import {Salida} from 'src/app/models/salida.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Remesa } from 'src/app/models/remesa.model';

@Injectable({
  providedIn: 'root'
})
export class CajaService {

  constructor(
    private aff: AngularFirestore
  ) { }

  obtenerCajasActivas(): Observable<CajaModel[]> {
    return this.aff.collection<CajaModel>('boxes', ref => ref.where('estado', '!=', '2')).valueChanges({idField: 'id'});
  }

  obtenerCajaClave(nombre: string): Observable<CajaModel[]> {
    return this.aff.collection<CajaModel>('boxes',
      ref => ref.where('vendedor.nombre', '==', nombre).where('estado', '!=', '2')
    ).valueChanges();
  }

  caja(id: string): Observable<CajaModel> {
    return this.aff.doc<CajaModel>(`boxes/${id}`).valueChanges({idField: 'id'});
  }

  crearCaja(caja: CajaModel) {
    return this.aff.collection('boxes').add(caja);
  }

  acutalizarEstado(id: string, estado: string) {
    return this.aff.doc(`boxes/${ id }`).update({estado});
  }

  actualizarCuadre(id: string, cuadre: CuadreModel) {
    return this.aff.doc(`boxes/${ id }`).update({cuadre});
  }

  actualizarSalida(id: string, salidas: Salida[]) {
    return this.aff.doc(`boxes/${id}`).update({salidas});
  }

  actualizarRemesas(id: string, remesas: Remesa[]) {
    return this.aff.doc(`boxes/${id}`).update({remesas});
  }

  actualizarFondo(id: string, fondo: number) {
    return this.aff.doc(`boxes/${id}`).update({fondo});
  }
}
