import { Component, OnInit } from '@angular/core';
import {CajaService} from './services/caja.service';
import {CajaModel} from '../models/caja.model';
import {AlertController} from '@ionic/angular';
import {Router, NavigationExtras} from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cajas',
  templateUrl: './cajas.page.html',
  styleUrls: ['./cajas.page.scss'],
})
export class CajasPage implements OnInit {

  cajas: Observable<CajaModel[]>;

  constructor(
    public cajaService: CajaService,
    private alertController: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.obtenerCajasActivas();

  }

  async obtenerCajasActivas() {
    try {
      this.cajas = this.cajaService.obtenerCajasActivas();
    } catch (error) {
      console.log(error.message);
    }
  }

  async acutalizarEstado(id: string, estado: string) {
    try {
      await this.cajaService.acutalizarEstado(id, estado);
    } catch (error) {
      console.log(error);
    }
  }

  async mostrarConfirm(id: string, estado: string) {
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: 'Seguro desea realizar esta accion?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'aceptar',
          handler: () => this.acutalizarEstado(id, estado)
        }
      ]
    });

    await alert.present();
  }

}
