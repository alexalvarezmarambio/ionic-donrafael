import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgregarRemesaPage } from './agregar-remesa.page';

const routes: Routes = [
  {
    path: '',
    component: AgregarRemesaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgregarRemesaPageRoutingModule {}
