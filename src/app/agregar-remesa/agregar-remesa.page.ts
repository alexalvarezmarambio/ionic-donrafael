import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';

import * as dayjs from 'dayjs';

import { CajaService } from '../cajas/services/caja.service';
import { CajaModel } from '../models/caja.model';
import { Remesa } from '../models/remesa.model';

@Component({
  selector: 'app-agregar-remesa',
  templateUrl: './agregar-remesa.page.html',
  styleUrls: ['./agregar-remesa.page.scss'],
})
export class AgregarRemesaPage implements OnInit {
  remesaForm = this.fb.group({
    monto: [0, [Validators.required, Validators.min(1)]]
  });

  caja: CajaModel;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private toast: ToastController,
    public boxService: CajaService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.boxService.caja(params.id).subscribe(
        res => this.caja = res
      );
    });
  }

  agregarRemesa() {
    if (this.remesaForm.invalid) {
      this.mostrarToast('Formulario Invalido');
      return;
    }

    const fecha = dayjs().format('YYYY-MM-DD HH:mm:ss');
    const monto = Number.parseInt(this.remesaForm.controls.monto.value, 10);
    const remesa: Remesa = {
      fecha,
      monto
    };
    this.caja.remesas.push(remesa);

    this.boxService.actualizarRemesas(this.caja.id, this.caja.remesas)
    .then(res => {
      this.mostrarToast('Remesa Agregada');
      this.remesaForm.reset();
    })
    .catch(error => this.mostrarToast(error));
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 8000,
      message: mensaje
    });

    toast.present();

  }

}
