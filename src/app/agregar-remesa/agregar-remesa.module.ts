import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarRemesaPageRoutingModule } from './agregar-remesa-routing.module';

import { AgregarRemesaPage } from './agregar-remesa.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    AgregarRemesaPageRoutingModule
  ],
  declarations: [AgregarRemesaPage]
})
export class AgregarRemesaPageModule {}
