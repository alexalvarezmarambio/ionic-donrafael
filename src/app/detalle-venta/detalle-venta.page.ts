import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VentaModel } from '../models/venta.model';
import { VentasService } from '../ventas/services/ventas.service';

@Component({
  selector: 'app-detalle-venta',
  templateUrl: './detalle-venta.page.html',
  styleUrls: ['./detalle-venta.page.scss'],
})
export class DetalleVentaPage implements OnInit {
  venta: VentaModel;

  constructor(
    private route: ActivatedRoute,
    public saleService: VentasService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.saleService.venta(params.id).subscribe(
        res => this.venta = res
      );
    });
  }

}
