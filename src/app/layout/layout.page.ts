import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.page.html',
  styleUrls: ['./layout.page.scss'],
})
export class LayoutPage implements OnInit {
  titulo = '';
  constructor(
    public main: MainService
  ) { }

  ngOnInit() {
    this.titulo = this.main.usuario.nombre;
  }

}
