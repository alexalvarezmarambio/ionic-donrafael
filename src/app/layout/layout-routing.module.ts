import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutPage } from './layout.page';

const routes: Routes = [
  {
    path: '',
    component: LayoutPage,
    children: [
      {
        path: '',
        loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardPageModule)
      },
      {
        path: 'producto',
        loadChildren: () => import('../producto/producto.module').then(m => m.ProductoPageModule)
      },
      {
        path: 'productos/:nombre',
        loadChildren: () => import('../productos/productos.module').then(m => m.ProductosPageModule)
      },
      {
        path: 'updateProducto/:id',
        loadChildren: () => import('../update-producto/update-producto.module').then(m => m.UpdateProductoPageModule)
      },
      {
        path: 'cajas',
        loadChildren: () => import('../cajas/cajas.module').then(m => m.CajasPageModule)
      },
      {
        path: 'salidas/:id',
        loadChildren: () => import('../salidas/salidas.module').then(m => m.SalidasPageModule)
      },
      {
        path: 'crear-caja',
        loadChildren: () => import('../crear-caja/crear-caja.module').then(m => m.CrearCajaPageModule)
      },
      {
        path: 'cuadre/:id',
        loadChildren: () => import('../cuadre/cuadre.module').then(m => m.CuadrePageModule)
      },
      {
        path: 'proveedores',
        loadChildren: () => import('../proveedores/proveedores.module').then(m => m.ProveedoresPageModule)
      },
      {
        path: 'nuevo-prov',
        loadChildren: () => import('../nuevo-prov/nuevo-prov.module').then(m => m.NuevoProvPageModule)
      },
      {
        path: 'categorias',
        loadChildren: () => import('../categorias/categorias.module').then( m => m.CategoriasPageModule)
      },
      {
        path: 'nueva-categoria',
        loadChildren: () => import('../nueva-categoria/nueva-categoria.module').then( m => m.NuevaCategoriaPageModule)
      },
      {
        path: 'update-categoria/:id',
        loadChildren: () => import('../update-categoria/update-categoria.module').then( m => m.UpdateCategoriaPageModule)
      },
      {
        path: 'update-prov/:id',
        loadChildren: () => import('../update-prov/update-prov.module').then( m => m.UpdateProvPageModule)
      },
      {
        path: 'vendedores',
        loadChildren: () => import('../vendedores/vendedores.module').then( m => m.VendedoresPageModule)
      },
      {
        path: 'nuevo-vendedor',
        loadChildren: () => import('../nuevo-vendedor/nuevo-vendedor.module').then( m => m.NuevoVendedorPageModule)
      },
      {
        path: 'update-vendedor/:id',
        loadChildren: () => import('../update-vendedor/update-vendedor.module').then( m => m.UpdateVendedorPageModule)
      },
      {
        path: 'remesas/:id',
        loadChildren: () => import('../remesas/remesas.module').then( m => m.RemesasPageModule)
      },
      {
        path: 'update-fondo/:id',
        loadChildren: () => import('../update-fondo/update-fondo.module').then( m => m.UpdateFondoPageModule)
      },
      {
        path: 'agregar-remesa/:id',
        loadChildren: () => import('../agregar-remesa/agregar-remesa.module').then( m => m.AgregarRemesaPageModule)
      },
      {
        path: 'ventas',
        loadChildren: () => import('../ventas/ventas.module').then( m => m.VentasPageModule)
      },
      {
        path: 'detalle-venta/:id',
        loadChildren: () => import('../detalle-venta/detalle-venta.module').then( m => m.DetalleVentaPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutPageRoutingModule {}
