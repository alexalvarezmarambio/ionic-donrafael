import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateProvPage } from './update-prov.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateProvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateProvPageRoutingModule {}
