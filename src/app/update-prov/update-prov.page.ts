import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ProveedorService } from '../proveedores/services/proveedor.service';

@Component({
  selector: 'app-update-prov',
  templateUrl: './update-prov.page.html',
  styleUrls: ['./update-prov.page.scss'],
})
export class UpdateProvPage implements OnInit {
  provForm = this.fb.group({
    id: [''],
    nombre: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private toast: ToastController,
    public provService: ProveedorService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.provService.proveedor(params.id).subscribe(
        res => this.provForm.setValue(res)
      );
    });
  }

  updateProv() {
    if (this.provForm.invalid) {
      this.mostrarToast('Formulario Invalido');
      return;
    }

    const prov = this.provForm.value;
    const {id, ...update} = prov;
    update.nombre = update.nombre.toUpperCase();

    this.provService.updateProveedor(id, update)
    .then(res => {
      this.mostrarToast('Proveedor Actualizado');
      this.provForm.reset();
    })
    .catch(error => this.mostrarToast(error))
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 8000,
      message: mensaje
    });

    toast.present();

  }

}
