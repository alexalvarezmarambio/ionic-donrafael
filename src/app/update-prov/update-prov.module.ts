import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateProvPageRoutingModule } from './update-prov-routing.module';

import { UpdateProvPage } from './update-prov.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    UpdateProvPageRoutingModule
  ],
  declarations: [UpdateProvPage]
})
export class UpdateProvPageModule {}
