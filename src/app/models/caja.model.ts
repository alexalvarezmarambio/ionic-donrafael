import {Remesa} from './remesa.model';
import {Salida} from './salida.model';
import { VendedorModel } from './vendedor.model';

export interface CajaModel {
  id?: string;
  apertura: string;
  cierre: string;
  fondo: number;
  estado: string;
  vendedor: VendedorModel;
  remesas: Remesa[];
  salidas: Salida[];
}
