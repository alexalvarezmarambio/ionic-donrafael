export interface VendedorModel {
   id?: string;
   nombre: string;
   clave: string;
   estado: boolean;
}
