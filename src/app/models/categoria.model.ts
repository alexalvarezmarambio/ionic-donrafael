export class Categoria {
  id?: string;
  nombre: string;
  estado: boolean;
  pos: boolean;
}