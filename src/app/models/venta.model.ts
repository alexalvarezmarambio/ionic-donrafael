import {CajaModel} from './caja.model';
import {ProductoModel} from './producto.model';

export interface VentaModel {
  id: string;
  fecha: string;
  folio: number;
  total: number;
  dte: number;
  caja: CajaModel;
  productos: ProductoModel[];
}
