import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductoModel } from '../models/producto.model';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { ProductoService } from '../productos/services/producto.service';
import { CategoryService } from '../categorias/service/category.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-update-producto',
  templateUrl: './update-producto.page.html',
  styleUrls: ['./update-producto.page.scss'],
})
export class UpdateProductoPage implements OnInit {

  loading = false;
  producto: ProductoModel;
  productoForm = this.fb.group({
    producto: ['', Validators.required],
    barcode: ['', Validators.required],
    precio: ['', [Validators.required, Validators.min(10)]],
    categoria: ['', Validators.required],
    medida: ['', Validators.required]
  });
  categorias: Observable<any>;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastController,
    public main: ProductoService,
    public categoryService: CategoryService
  ) {}

  ngOnInit() {
    this.categorias = this.categoryService.categorias();
    this.route.params.subscribe( params => {
      this.main.obtenerProductoId(params.id).subscribe(
        (res) => {
          this.producto = res;
          this.setForm();
        }
      );
    });
  }

  setForm() {
    this.productoForm.controls.barcode.setValue(this.producto.barcode);
    this.productoForm.controls.producto.setValue(this.producto.name);
    this.productoForm.controls.categoria.setValue(this.producto.category);
    this.productoForm.controls.precio.setValue(this.producto.kilo <= 10 ? this.producto.precio : this.producto.kilo);
    this.productoForm.controls.medida.setValue(this.producto.kilo <= 10 ? 'U' : 'K');
  }

  async actualizar() {
    try {
      const medida = this.productoForm.controls.medida.value;
      this.loading = true;
      const body = {
        name: this.productoForm.controls.producto.value.toString().toUpperCase(),
        kilo : medida === 'K' ? Number.parseInt(this.productoForm.controls.precio.value.toString()) : 0,
        precio : medida === 'U' ? Number.parseInt(this.productoForm.controls.precio.value.toString()) : 0,
        category: this.productoForm.controls.categoria.value
      };

      await this.main.actualizarProducto(this.producto.id, body);
      this.mostrarToast('Producto Actualizado.');
      this.router.navigateByUrl(`/layout/productos/${this.producto.category}`);
      this.loading = false;

    } catch (error) {
      this.mostrarToast(error.message);
      this.loading = false;
    }
  }

  async mostrarToast(mensaje: string) {

    const toast = await this.toast.create({
      duration: 3000,
      message: mensaje
    });

    toast.present();

  }

}
